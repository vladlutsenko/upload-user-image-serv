import express from "express";
const jwt = require('jsonwebtoken');
const { secret } = require('../../config/server');
import HttpException from '../exceptions/HttpException';

export default function verification(req: express.Request, res: express.Response, next: express.NextFunction) {
    if (req.url!=='/user/login') {
        const token = req.headers.authorization;
      
        if (token) {
          jwt.verify(token, secret, (err: any, user: any) => {
            if (err) {
              next(new HttpException(403, "Forbidden"));
            }
            next();
          });
        }
        else{
          next(new HttpException(400, "JWT missing"));
        }
    }
    else{
        next();
    }
  
}
