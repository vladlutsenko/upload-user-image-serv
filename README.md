# How to use

1. Make sure that folder src/uploads exists
2. Make sure that .env file filled correctly
3. Run docker-compose up command to start server and db

To add image to existing user, you should send image file to this endpoint:
```
http://localhost:1337/user/upload/:userId
```

Get users list:
```
http://localhost:1337/user/
```

Static server with images:
```
http://localhost:1337/images/:filename
```