'use strict';

const { Sequelize } = require('../models');
const bcrypt = require('bcrypt');

async function hashPassword(password) {
  const hashedPassword = await bcrypt.hash(password, 10);
  return hashedPassword;
}


module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
     await queryInterface.bulkInsert('User', [{
      firstName: 'Test user',
      lastName: 'James',
      email: 'test@email.com',
      password: await hashPassword('1234'),
      createdAt: Sequelize.literal('CURRENT_TIMESTAMP'), 
      updatedAt: Sequelize.literal('CURRENT_TIMESTAMP')
    }], {});

  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
     await queryInterface.bulkDelete('User', {
      email: 'test@email.com'
     }, {});
  }
};
