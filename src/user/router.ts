import express from 'express';
import multer from 'multer';
import * as path from 'path';
import * as controller from './controller'
import HttpException from '../exceptions/HttpException';
import { nextTick } from 'process';

const router = express.Router();


router.post('/login', async (req: express.Request, res: express.Response, next: express.NextFunction) => {
    try {
        const loggedToken = await controller.loginUser(req.body.email, req.body.password);
        res.json({
            loggedToken
        }); 
    } catch (error) {
        next(error);
    }
});

router.post('/register', async (req: express.Request, res: express.Response, next: express.NextFunction) => {
    try {
        const newuser = await controller.registerUser(req.body);
        res.json({
            newuser
        });
    } catch (error) {
        next(new HttpException(400, 'Registartion error'));
    }
});

router.get('/', async (req: express.Request, res: express.Response) => {
    const users = await controller.getAllUsers();
    res.json({
        users
    });
});


const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, './src/uploads/');
    },
    filename: function (req, file, cb) {
      cb(null, Date.now() + path.extname(file.originalname)); //Appending extension
    }
});
  
const upload = multer({ storage: storage });

router.post('/upload/:id',upload.single('file'), async (req, res) => {
    console.log(req.file?.filename);
    if (req.file?.filename) {
        const user = await controller.addImage(req.params.id, req.file.filename);
        res.json({
            user
        });
    }
});


export default router;