const db = require('../../db/models');
import * as service from './service';
import User from './interface';


export async function registerUser(user: User) {
    const newuser = await service.registerUser(user);
    return newuser;
}

export async function loginUser(email: string, password: string) {
  const token = await service.loginUser(email, password);
  return token;
}

export async function getAllUsers() {
  const users = await service.getAllUsers();
  return users;
}

export async function addImage(id: string, filename: string) {
  const user = await service.addImage(id, filename);
  return user;
}
